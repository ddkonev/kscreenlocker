# translation of kscreenlocker_greet.pot to Esperanto
# Copyright (C) 2022 Free Software Foundation, Inc.
# This file is distributed under the same license as the kscreenlocker package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kscreenlocker\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-25 02:02+0000\n"
"PO-Revision-Date: 2023-05-06 19:01+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: main.cpp:113
#, kde-format
msgid "Greeter for the KDE Plasma Workspaces Screen locker"
msgstr "Salutilo por la KDE Plasma Workspaces Ekranŝlosilo"

#: main.cpp:117
#, kde-format
msgid "Starts the greeter in testing mode"
msgstr "Lanĉas la salutilon en prova reĝimo"

#: main.cpp:120
#, kde-format
msgid "Starts the greeter with the selected theme (only in Testing mode)"
msgstr "Lanĉas la salutilon kun la elektita temo (nur en Testa reĝimo)"

#: main.cpp:124
#, kde-format
msgid "Lock immediately, ignoring any grace time etc."
msgstr "Ŝlosi tuj, ignorante ajnan gracan tempon ktp."

#: main.cpp:126
#, kde-format
msgid "Delay till the lock user interface gets shown in milliseconds."
msgstr "Prokrasto ĝis la ŝlosila uzantinterfaco montriĝas en milisekundoj."

#: main.cpp:129
#, kde-format
msgid "Don't show any lock user interface."
msgstr "Ne montri ajnan seruran uzantinterfacon."

#: main.cpp:130
#, kde-format
msgid "Default to the switch user UI."
msgstr "Defaŭlte al la ŝanĝa uzanto UI."

#: main.cpp:132
#, kde-format
msgid "File descriptor for connecting to ksld."
msgstr "Dosiera priskribilo por konekti al ksld."
